
  const check = ( ) => {
  	let txt = document.getElementById('text');
  	let date = document.getElementById('date');
  	let radio = document.getElementById('radio');
  	let checkbox = document.getElementById('checkbox');
  	let select = document.getElementById('select');
  	let password = document.getElementById('password');

  	let msg_txt = document.getElementById('msg_text');
  	let msg_date = document.getElementById('msg_date');
  	let msg_radio = document.getElementById('msg_radio');
  	let msg_checkbox = document.getElementById('msg_checkbox');
  	let msg_select = document.getElementById('msg_select');
  	let msg_password = document.getElementById('msg_password');

  	if (txt.value.trim().length < 5 || !txt.value || !(txt.value).match(/[^a-zA-Z]/)) {
  		msg_txt.textContent = 'nope';
  		msg_txt.style.color = 'red';
  	} else {
  	  	msg_txt.textContent = 'yup';
  		msg_txt.style.color = 'green';
  	}
  	
  	if (date.value) {
  	  	msg_date.textContent = 'yup';
  		msg_date.style.color = 'green';
  	} else {
  		msg_date.textContent = 'nope';
  		msg_date.style.color = 'red';
  	}

  	if (radio.checked) {
  	  	msg_radio.textContent = 'yup';
  		msg_radio.style.color = 'green';
  	} else {
  		msg_radio.textContent = 'nope';
  		msg_radio.style.color = 'red';
  	}

  	if (checkbox.checked) {
  	  	msg_checkbox.textContent = 'yup';
  		msg_checkbox.style.color = 'green';
  	} else {
  		msg_checkbox.textContent = 'nope';
  		msg_checkbox.style.color = 'red';

  	}

  	if (select.value == '-=Выбери таблетку=-') {
  		msg_select.textContent = 'nope';
  		msg_select.style.color = 'red';

  	} else {
  	  	msg_select.textContent = 'yup';
  		msg_select.style.color = 'green';
  	}

  	if (password.value.length < 15 || !(password.value).match(/\d/) || !(password.value).match(/[a-z]/) || !(password.value).match(/[A-Z]/) || !(password.value).match(/[^a-zA-Z\d]\w/) || (password.value).match(/\s/)) {
  	  	msg_password.textContent = 'nope';
  		msg_password.style.color = 'red';
  	} else {
  	  	msg_password.textContent = 'yup';
  		msg_password.style.color = 'green';
  	}
}

  document.getElementById("submit_btn").onclick = check;